extends RigidBody

func _ready():
	randomize()
	var ball_color = Color(randf(), randf(), randf())
	var ball_material = SpatialMaterial.new()
	ball_material.albedo_color = ball_color
	ball_material.emission = ball_color
	ball_material.emission_enabled = true
	$MeshInstance.set_surface_material(0, ball_material)
	$OmniLight.light_color = ball_color
	
	
