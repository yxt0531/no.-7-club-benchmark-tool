extends Spatial

var is_benchmarking = false
var framerates = []

func _process(delta):
	$GUI/LabelCurrentFPS.text = "FPS: " + str(Engine.get_frames_per_second())
	if is_benchmarking:
		framerates.append(Engine.get_frames_per_second())

func _on_ButtonStart_button_down():
	for reb in $REBs.get_children():
		reb.queue_free()
	
	is_benchmarking = true
	framerates.clear()
	
	$GUI/ButtonStart.visible = false
	$GUI/LabelResult.visible = false
	
	$AnimationPlayer.play("benchmark")

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "benchmark":
		framerates.remove(0)
		framerates.remove(0)
		framerates.remove(0)
		framerates.remove(0)
		
		var total = 0
		for framerate in framerates:
			total += framerate
			
		var average = total / framerates.size()
		
		var lowest = framerates.min()
		var highest = framerates.max()
		
		$GUI/LabelResult.text = "Average: " + str(int(average)) + "\nHighest: " + str(highest) + "\nLowest: " + str(lowest)
		
		$GUI/ButtonStart.visible = true
		$GUI/LabelResult.visible = true
