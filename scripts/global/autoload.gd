extends Node

func _ready():
	OS.set_window_maximized(true)

func _process(delta):
	if Input.is_action_just_pressed("toggle_fullscreen"):
		if not OS.window_borderless:
			OS.set_window_maximized(false)
			OS.window_borderless = true
			OS.set_window_size(OS.get_screen_size())
			OS.set_window_position(Vector2(0, 0))
		else:
			OS.window_borderless = false
			OS.set_window_size(OS.get_screen_size())
			OS.set_window_position(Vector2(0, 0))
			OS.set_window_maximized(true)
