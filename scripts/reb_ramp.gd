extends StaticBody

func spawn_ball():
	var reb = load("res://scenes/rigid_emissive_ball.tscn").instance()
	reb.translation = to_global($SpawnPosition.translation)
	get_parent().get_node("REBs").add_child(reb)
