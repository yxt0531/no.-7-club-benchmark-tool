extends Spatial

var red_emissive
var cyan_emissive

var is_increasing = false

func _ready():
	red_emissive = $Cyberlobby.get_surface_material(2)
	cyan_emissive = $Cyberlobby.get_surface_material(3)
	
func _process(delta):
	if is_increasing:
		if red_emissive.emission_energy >= 2 and cyan_emissive.emission_energy >= 2:
			is_increasing = not is_increasing
		else:
			if red_emissive.emission_energy < 2:
				red_emissive.emission_energy += delta
			if cyan_emissive.emission_energy < 2:
				cyan_emissive.emission_energy += delta
	else:
		if red_emissive.emission_energy <= 0.05 and cyan_emissive.emission_energy <= 0.05:
			is_increasing = not is_increasing
		else:
			if red_emissive.emission_energy > 0.05:
				red_emissive.emission_energy -= delta
			if cyan_emissive.emission_energy > 0.05:
				cyan_emissive.emission_energy -= delta
				
